let componentStyles = `
#slider-container {
  --value : 0;
  --slider-track-color : #6200ee38;
  --slider-thumb-color : #6200ee;
  --slider-fill-color  : #6200ee;

  width : 100%;
  height: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0;
  margin: 0;
}


#slider {
  -webkit-appearance: none;
  appearance: none;
  height: 1rem;
  width: 100%;
  margin : 0;
  padding: 0;
  background-color: #00000000;
  outline: none;
  z-index: 99;
  cursor: pointer;
}

#slider:hover::-webkit-slider-thumb {
  -webkit-box-shadow: 0 0 0 8px #6200ee15;
  box-shadow: 0 0 0 8px #6200ee15;
  border-radius: 50%;
}

#slider:focus::-webkit-slider-thumb {
  -webkit-box-shadow: 0 0 0 8px #6200ee30;
  box-shadow: 0 0 0 8px #6200ee30;
  border-radius: 50%;
}

#slider:active::-webkit-slider-thumb {
  -webkit-box-shadow: 0 0 0 8px #6200ee40;
  box-shadow: 0 0 0 8px #6200ee40;
  border-radius: 50%;
}

#slider-track {
  position: absolute;
  top: calc(50% - 0.25rem);
  left: 0;
  width: 100%;
  height: 6px;
  border-radius: 0.25rem;
  background-color: var(--slider-track-color);
  overflow: hidden ;
}

#slider-track::before {
  position: absolute;
  content: "";
  left: calc(-100% + 1.5rem);
  top : 0;
  width : calc(100% - 1rem);
  height: 100%;
  background-color: var(--slider-fill-color);
  transition: background-color 300ms ease-out;
  transform-origin: 100% 0%;
  transform: translateX(calc( var(--value) * 100% )) scaleX(1.2);
}

#slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width : 20px;
  height: 20px;
  border-radius: 50%;
  background-color: var(--slider-thumb-color);
  z-index: 99;
}

#slider::-webkit-slider-thumb:focus {
  display: none;
}

#value {
  position: absolute ;
  bottom: calc(100% + 0.5rem) ;
  left: calc( var(--value) * calc(100% - 1rem) - 0.5rem);
  min-width: 1rem;
  border-radius: 4px;
  pointer-events: none;
  padding: 0.4rem;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #FFF;
  font-size: .7rem;
  background-color: var(--slider-fill-color);
  opacity: 0;
  transition: left 10ms ease-out , opacity 30ms 300ms ease-out , background-color 300ms ease-out ;
}

#value::before {
  position: absolute ;
  content: "" ;
  top: 100% ;
  left: 50% ;
  width: 1rem ;
  height: 1rem ;
  border-radius: 2px ;
  background-color: inherit ;
  transform: translate(-50%,-80%) rotate(45deg);
  z-index: -1 ;
}

#slider-container:hover #value {
  opacity: 1;
}
`;

class rangeSlider extends HTMLElement {
  constructor() {
    super();
    this.value = parseFloat(this.getAttribute("value")) || 50;
    this.min = parseFloat(this.getAttribute("min")) || 0;
    this.max = parseFloat(this.getAttribute("max")) || 100;
    this.step = parseFloat(this.getAttribute("step")) || 1;

    this.style.minWidth = "20rem";
    this.style.position = "relative";

    this.root = this.attachShadow({ mode: "open" });

    this.dragging = false;

    this.create();
    this.update();
  }

  create() {
    let slider = document.createElement("input");
    let sliderContainer = document.createElement("div");
    let sliderTrack = document.createElement("div");
    let value = document.createElement("div");

    let style = document.createElement("style");
    style.innerHTML = componentStyles;

    slider.type = "range";
    slider.role = "range";
    slider.tabIndex = "0";
    slider.ariaOrientation = "horizontal";
    slider.id = "slider";
    slider.min = this.min;
    slider.max = this.max;
    slider.step = this.step;
    slider.value = this.value;

    sliderContainer.id = "slider-container";
    sliderTrack.id = "slider-track";
    value.id = "value";

    slider.addEventListener("input", this.update.bind(this));

    sliderContainer.appendChild(slider);
    sliderContainer.appendChild(value);
    sliderContainer.appendChild(sliderTrack);
    this.root.appendChild(style);
    this.root.appendChild(sliderContainer);
  }

  update() {
    let track = this.root.getElementById("slider-container");
    let slider = this.root.getElementById("slider");
    let value = this.root.getElementById("value");
    let valuePercentage = slider.value / (this.max - this.min);
    value.innerText = slider.value;
    track.style.setProperty("--value", valuePercentage);
  }
}

window.customElements.define("range-slider", rangeSlider);
